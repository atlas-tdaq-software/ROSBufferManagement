// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: BUFFER
  Authors: ATLAS ROS group 	

  $Log$
  Revision 1.13  2008/02/08 17:36:47  gorini
  Added possibility to append buffers from an offset and/or for a limited size

  Revision 1.12  2007/05/02 13:40:45  gorini
  changed max number of pages for Buffer

  Revision 1.11  2006/03/08 15:30:12  gorini
  Made the argument og operator- a const Buffer& instead of Buffer&

  Revision 1.10  2003/10/16 19:17:02  gorini
  Revert to old behaviour where a newly created buffer has an initial page associated to it even before any memory is reserved

  Revision 1.9  2003/10/16 12:40:29  gorini
  On call of begin() an empty Buffer will get a page. To ensure proper functioning of ROBFragment constructor. Eventually the ROB fragment shall be changed instead of the Buffer.

  Revision 1.8  2003/10/15 12:01:27  gorini
  Added clear method to Buffer

  Revision 1.7  2003/10/14 17:48:10  gorini
  Added copy constructor/operator for buffer

  Revision 1.6  2003/05/13 07:11:55  gorini
  Added current() and reserved() methods.

  Revision 1.5  2002/11/26 13:42:19  gorini
  added DCWrapperBuffer which is used to translate a DC Buffer to a ROS buffer without data copy

  Revision 1.4  2002/11/12 15:53:52  gorini
  New constructor in Buffer and correct catch of exceptions in test_buffer program.

  Revision 1.3  2002/11/06 21:11:08  gorini
  New Exception schema

  Revision 1.2  2002/10/08 14:57:54  pyannis
  cleanup

  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
  Initial version of DataFlow ROS packages tree...

*/

#ifndef BUFFER_H
#define BUFFER_H

#include <new>
#include <iostream>
#include <iomanip>
#include <vector>

#include "ROSObjectAllocation/FastObjectPool.h"

#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/MemoryPool.h"

namespace ROS {
 
  class Buffer {
  public:
    enum {MAX_NUMBER_OF_PAGES = 2000};
    typedef const MemoryPage * const * page_iterator;
    Buffer();
    Buffer(MemoryPool *memoryPool);
    Buffer(MemoryPage *memoryPage,MemoryPool *memoryPool=0);
    Buffer(Buffer &other,MemoryPool *memoryPool=0);
    ~Buffer();
    u_int size() const;
    u_int append(MemoryPage & memoryPage, u_int offset=0);
    u_int append(MemoryPage & memoryPage, u_int offset, u_int maxSize);
    u_int append(Buffer & other, u_int offset=0);
    u_int append(Buffer & other, u_int offset, u_int maxSize);
    void * reserve(u_int size, u_int minSize=0);
    void release(u_int size);
    int numberOfPages() const;
    u_int pageSize();
    void print() const;
    page_iterator begin() const;
    page_iterator end() const;
    void * current();
    u_int reserved();
    void clear();

    void * operator new(size_t);
    void operator delete(void *memory);
    Buffer & operator=(const Buffer &other);

  private:
    MemoryPool * m_memoryPool;
    u_int m_size;
    MemoryPage * m_lastMemoryPage;
    u_int m_pageSize;
    u_int m_numberOfPages; 
    void * m_current;
    u_int m_reserved;
    /**
     * @link aggregationByValue
     * @supplierCardinality 1..* 
     */
    MemoryPage *m_pages[MAX_NUMBER_OF_PAGES];
    bool allocateNewPage();
  };
     
  inline Buffer::~Buffer() {
    for (u_int i=0; i<m_numberOfPages; i++) {
      m_pages[i]->free();
    }   
  }
  
  inline u_int Buffer::size() const {
    return m_size;
  }
  
  inline Buffer::page_iterator Buffer::begin() const {
    return m_pages;
  }
  
  inline Buffer::page_iterator Buffer::end() const {
    return &(m_pages[m_numberOfPages]);
  }

  inline void * Buffer::operator new(size_t) {
    return FastObjectPool<Buffer>::allocate(); //This is thread UNSAFE!
  }

  inline void Buffer::operator delete(void * memory) {
    FastObjectPool<Buffer>::deallocate(memory); //This is thread UNSAFE!
  }

  inline void Buffer::release(u_int size) {
    if (m_lastMemoryPage!=0) {
      int actualSize =  m_lastMemoryPage->release(size); 
      m_size -= actualSize; 
      m_reserved -= actualSize;
    }
  }

  inline int Buffer::numberOfPages() const {
    return m_numberOfPages;
  }

  inline void * Buffer::current() {
    return m_current;
  }

  inline u_int Buffer::reserved() {
    return m_reserved;
  } 

  inline u_int Buffer::pageSize() {
    return m_pageSize;
  }

  inline bool Buffer::allocateNewPage() {
    bool rf = false;
    if (m_memoryPool!=0) {
      m_lastMemoryPage = m_memoryPool->getPage();
      m_pages[m_numberOfPages] = m_lastMemoryPage;
      m_numberOfPages++;    
      rf = true;
    }
    return rf;
  }
}

inline void * operator new(size_t size,ROS::Buffer * buffer) {
  return buffer->reserve((u_int)size,(u_int)size);
}

inline void * operator new[](size_t size,ROS::Buffer * buffer) {
  return buffer->reserve((u_int)size,(u_int)size);
}

#endif //BUFFER_H
