// -*- c++ -*- 
/*
  ATLAS ROS Software

  Class: DCWRAPPERBUFFER
  Authors: ATLAS ROS group 	


*/

#ifndef DCWRAPPERBUFFER_H
#define DCWRAPPERBUFFER_H

#include <new>
#include <vector>

#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSBufferManagement/Buffer.h"
#include "msg/Buffer.h"
#include "ROSMemoryPool/MemoryPage.h"

namespace ROS {
  class DCWrapperBuffer : public Buffer 
  {
  public:
    void * operator new(size_t) ;
    void operator delete(void *memory) ;
    DCWrapperBuffer(MessagePassing::Buffer & dcBuffer) ;

  private:
    static void releaseFunction(MemoryPage *memoryPage, void *buffer) ; 
  };


  inline void * DCWrapperBuffer::operator new(size_t) {
    return FastObjectPool<DCWrapperBuffer>::allocate() ; //This is thread UNSAFE!
  }

  inline void DCWrapperBuffer::operator delete(void * memory) {
    FastObjectPool<DCWrapperBuffer>::deallocate(memory); //This is thread UNSAFE!
  }

};

#endif //DCWRAPPERBUFFER_H
