/*
  ATLAS ROS Software

  Class: BUFFERMANAGEMENTEXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef BUFFERMANAGEMENTEXCEPTION_H
#define BUFFERMANAGEMENTEXCEPTION_H

#include "DFExceptions/ROSException.h"

namespace ROS {
  class BufferManagementException : public ROSException {    
  public:
    enum ErrorCode { UNCLASSIFIED, 
		     TOO_LARGE_RESERVATION, 
		     MAX_NUMBER_OF_PAGES_EXCEEDED} ; 

    BufferManagementException(ErrorCode error) ;
    BufferManagementException(ErrorCode error, std::string description) ;
    BufferManagementException(ErrorCode error, const ers::Context& context) ;
    BufferManagementException(ErrorCode error, std::string description, const ers::Context& context) ;
    BufferManagementException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

  protected:
    virtual std::string getErrorString(u_int errorId) const;
    virtual ers::Issue * clone() const { return new BufferManagementException( *this ); }
    static const char * get_uid() {return "ROS::BufferManagementException";}
    virtual const char* get_class_name() const {return get_uid();}
  };
  
  inline BufferManagementException::BufferManagementException(BufferManagementException::ErrorCode error)
    : ROSException("ROSBufferManagement",error, getErrorString(error)) { }
   
  inline BufferManagementException::BufferManagementException(BufferManagementException::ErrorCode error, std::string description)
    : ROSException("ROSBufferManagement",error, getErrorString(error),description) { }

  inline BufferManagementException::BufferManagementException(BufferManagementException::ErrorCode error, const ers::Context& context)
    : ROSException("ROSBufferManagement",error, getErrorString(error), context) { }
   
  inline BufferManagementException::BufferManagementException(BufferManagementException::ErrorCode error, std::string description, const ers::Context& context)
    : ROSException("ROSBufferManagement",error, getErrorString(error),description, context) { }

inline BufferManagementException::BufferManagementException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSBufferManagement", error, getErrorString(error), description, context) {}

  inline std::string BufferManagementException::getErrorString(u_int errorId) const {
	  std::string rc;    
    switch (errorId) {  
    case UNCLASSIFIED:
      rc = "Unclassified error" ;
      break;
    case TOO_LARGE_RESERVATION:
      rc = "The space requested exceeds the size of a memory page" ;
      break;
    case MAX_NUMBER_OF_PAGES_EXCEEDED:
      rc = "Max number of pages exceeded" ;
      break;
    default:
      rc = "" ;
      break;
    }
    return rc;
  }
}
#endif //BUFFERMANAGEMENTEXCEPTION_H
