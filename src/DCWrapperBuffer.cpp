// -*- c++ -*- 
/*
  ATLAS ROS Software

  Class: DCWRAPPERBUFFER
  Authors: ATLAS ROS group 	
*/

#include "ROSBufferManagement/DCWrapperBuffer.h"
#include "ROSMemoryPool/WrapperMemoryPage.h"
#include "msg/BufferDescriptor.h"

using namespace ROS;

DCWrapperBuffer::DCWrapperBuffer(MessagePassing::Buffer & dcBuffer) {
  MemoryPage *page;
  void *parameter = (void *)&dcBuffer;
  for (MessagePassing::Buffer::descr_iterator it=dcBuffer.descr_begin(); it!=dcBuffer.descr_end(); it++) {
    page = new WrapperMemoryPage((char *)(*it).addr(),(*it).size(), 0,DCWrapperBuffer::releaseFunction, parameter);
    parameter = 0;
    append(*page);
    page->free();
  }
}

void DCWrapperBuffer::releaseFunction(MemoryPage *memoryPage,void *buffer) {
  if (buffer!=0) {
    MessagePassing::Buffer * dcBuffer = reinterpret_cast<MessagePassing::Buffer *>(buffer);
    delete dcBuffer;
  }
  WrapperMemoryPage *wMemoryPage = static_cast<WrapperMemoryPage *>(memoryPage);
  delete wMemoryPage;
}
