/*
  ATLAS ROS Software

  Class: BUFFER
  Authors: ATLAS ROS group 	
*/

#include <sstream>
#include "ROSBufferManagement/Buffer.h"
#include "ROSBufferManagement/BufferManagementException.h"
#include "ROSMemoryPool/VirtualMemoryPage.h"

using namespace ROS;

u_int Buffer::append(Buffer & other, u_int offset, u_int maxSize) {
  u_int remainingSize=maxSize;
  for (u_int i=0; i<other.m_numberOfPages && remainingSize>0; i++) {
    MemoryPage * nextPage = (other.m_pages)[i];
    u_int nextSize=nextPage->usedSize();
    if (offset>=nextSize) {
      offset -= nextSize;
    }
    else {
      remainingSize -= append(*nextPage,offset,remainingSize);
      offset = 0;
    }
  }
  return maxSize-remainingSize;
}

u_int Buffer::append(Buffer & other, u_int offset) {
  return append(other, offset, other.size());
}

u_int Buffer::append(MemoryPage & memoryPage, u_int offset) {
  return append(memoryPage,offset,memoryPage.usedSize());
}  

u_int Buffer::append(MemoryPage & memoryPage, u_int offset, u_int maxSize) {
  //Check if we have reached the maximum number of pages
  if (m_numberOfPages==MAX_NUMBER_OF_PAGES) 
    throw BufferManagementException(BufferManagementException::MAX_NUMBER_OF_PAGES_EXCEEDED, "I cannot append a new page");

  //Freeze the buffer
  if (m_lastMemoryPage!=0) m_lastMemoryPage->truncate();
  m_current=0;
  m_reserved=0;

  //Append new page
  MemoryPage *nextPage=&memoryPage;
  u_int availableSize=nextPage->usedSize();
  u_int appendedSize=0;
  if (offset<availableSize) {
    appendedSize = (maxSize < availableSize-offset ? maxSize : availableSize-offset);
    if (offset!=0 || maxSize!=availableSize) {
      nextPage = new VirtualMemoryPage(nextPage, offset, appendedSize);
    }
    else {
      nextPage->lock();
    }
    m_lastMemoryPage = nextPage;
    m_size += appendedSize;
    m_pages[m_numberOfPages] = m_lastMemoryPage;    
    m_numberOfPages++;
  }
  return appendedSize;
}

void * Buffer::reserve(u_int size, u_int minSize) {
  if (size > m_pageSize) {
    std::ostringstream msg;
    msg << " requested size = " << size << ", pageSize = " << m_pageSize << std::ends;
    throw BufferManagementException(BufferManagementException::TOO_LARGE_RESERVATION, msg.str());
  }

  if (m_lastMemoryPage == 0) {
    m_lastMemoryPage = m_memoryPool->getPage();
    m_pages[m_numberOfPages] = m_lastMemoryPage;
    m_numberOfPages++;    
  }
  else if (size > m_lastMemoryPage->capacity()) {
    if (m_numberOfPages==MAX_NUMBER_OF_PAGES) 
      throw BufferManagementException(BufferManagementException::MAX_NUMBER_OF_PAGES_EXCEEDED,
				      "I cannot reserve the requested space");
    m_lastMemoryPage->truncate();
    m_lastMemoryPage = m_memoryPool->getPage();
    m_pages[m_numberOfPages] = m_lastMemoryPage;
    m_numberOfPages++;
  }
  m_size += size;
  m_reserved = size;
  return (m_current = m_lastMemoryPage->reserve(size,minSize));
}

void Buffer::print() const {
  std::cout << "Buffer size = " << m_size << std::endl; 
  std::cout << " # Memory Segments = " << std::setw(4) << this->m_numberOfPages <<
    "     address      size  capacity  reference count" << std::endl;
  
  for (u_int i = 0; i < m_numberOfPages; i++) {
    std::cout << "                    " 
	      << std::hex << m_pages[i]->address() 
	      << std::setw(10) << std::dec << m_pages[i]->usedSize() 
	      << std::setw(10) << std::dec << m_pages[i]->capacity() 
	      << std::setw(10) << std::dec << m_pages[i]->referenceCount() 
	      << std::endl;
  }
}

Buffer::Buffer() 
  : m_memoryPool(0),
    m_size(0),
    m_lastMemoryPage(0),
    m_pageSize(0),
    m_numberOfPages(0), 
    m_current(0),
    m_reserved(0)
{
}

Buffer::Buffer(MemoryPool *memoryPool) 
  : m_memoryPool(memoryPool),
    m_size(0),
    m_lastMemoryPage(m_memoryPool->getPage()),
    m_pageSize(m_memoryPool->pageSize()),
    m_numberOfPages(1),
    m_current(0),
    m_reserved(0)
{
  m_pages[0]=m_lastMemoryPage;
}

Buffer::Buffer(MemoryPage *memoryPage,MemoryPool *memoryPool) 
  : m_memoryPool(memoryPool),
    m_size(memoryPage->usedSize()),
    m_lastMemoryPage(memoryPage),
    m_pageSize(0), 
    m_numberOfPages(1), 
    m_current(0),
    m_reserved(0)
{    
  m_pages[0]=m_lastMemoryPage;
  m_lastMemoryPage->lock();
  if (m_memoryPool!=0) {     
    m_pageSize = m_memoryPool->pageSize();
  }
}

Buffer::Buffer(Buffer &other,MemoryPool *memoryPool)
  : m_memoryPool(memoryPool),
    m_size(other.m_size),
    m_lastMemoryPage(other.m_lastMemoryPage),
    m_pageSize(0),
    m_numberOfPages(other.m_numberOfPages),
    m_current(0),
    m_reserved(0) 
{
  for (u_int i=0;i<m_numberOfPages;i++) {
    m_pages[i] = other.m_pages[i];
    m_pages[i]->lock();
  }
  if (m_memoryPool!=0) {     
    m_pageSize = m_memoryPool->pageSize();
  }
}
  
Buffer & Buffer::operator=(const Buffer &other) {
  if (&other != this) {
    for (u_int i=0;i<m_numberOfPages;i++) {
      m_pages[i]->free();
    }
    m_size = other.m_size;
    m_lastMemoryPage = other.m_lastMemoryPage;
    m_numberOfPages = other.m_numberOfPages;
    m_current = 0;
    m_reserved = 0; 
    for (u_int i=0;i<m_numberOfPages;i++) {
      m_pages[i] = other.m_pages[i];
      m_pages[i]->lock();
    }  
  }
  return (*this);
}

void Buffer::clear() {
  for (u_int i=0;i<m_numberOfPages;i++) {
    m_pages[i]->free();
  }  
  m_size = 0;
  m_current = 0;
  m_reserved = 0; 
  if (m_memoryPool!=0) {
    m_lastMemoryPage = m_memoryPool->getPage();
    m_pages[0]=m_lastMemoryPage;
    m_numberOfPages = 1;
  }
  else {
    m_lastMemoryPage = 0;
    m_numberOfPages = 0;
  }
}
