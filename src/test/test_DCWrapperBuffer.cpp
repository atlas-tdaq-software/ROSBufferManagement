
#include "ROSBufferManagement/Buffer.h"
#include "ROSBufferManagement/DCWrapperBuffer.h"
#include "msg/Buffer.h"
#include "msg/BufferManager.h"
#include "msg/BufferDescriptor.h" 

int main () {
  using namespace ROS;
  char * pointer ;
  
  MessagePassing::Buffer * dcBuffer = new MessagePassing::Buffer(1024) ;
  MessagePassing::Buffer::iterator it = dcBuffer->begin() ; 
  std::cout << "Created a DC Buffer " << std::endl ; 
 
  pointer = new char[20] ; 
  dcBuffer->insert(it,(void *)pointer,20,0,0) ;
  std::cout << "Allocated 20 characters at address: " << std::hex << (void *)&pointer[0] 
       << " and inserted in DC Buffer " << std::endl ;  

  pointer = new char[20] ; 
  dcBuffer->insert(it,(void *)pointer,20,0,0) ;
  std::cout << "Allocated 20 characters at address: " << std::hex << (void *)&pointer[0]   
       << " and inserted in DC Buffer " << std::endl ;  

  std::cout << "Wrapped the DC Buffer into a ROS Buffer: " << std::endl ;
  Buffer *rosBuffer = new DCWrapperBuffer(*dcBuffer) ;
  rosBuffer->print() ;
  delete rosBuffer;
  std::cout << "Deleted the Buffers" << std::endl ; 
}
