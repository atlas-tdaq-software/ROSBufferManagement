#include "ROSBufferManagement/Buffer.h" 
#include "ROSMemoryPool/MemoryPool_malloc.h" 
#include "ROSMemoryPool/MemoryPoolException.h" 
#include "ROSBufferManagement/BufferManagementException.h"
#include "ROSMemoryPool/MemoryPoolException.h"
#include "DFExceptions/ROSException.h"

using namespace ROS;

typedef struct s_test {
  int intPar ;
  long longPar ;
  char stringPar[7] ;  
} Test;

int main() { 
  //First lets create a memory pool
  MemoryPool * pool = new MemoryPool_malloc(10,4096);
  std::cout << "Created pool with 100 pages of size " << pool->pageSize() << std::endl ;

  //First we create some data structures and find out where 
  //they are put 
  Buffer * b = new Buffer(pool) ; 
  std::cout << "Buffer b created" << std::endl ;
  Test * t1 = new(b) Test ;
  std::cout << "Test structure 1 created on buffer b" << std::endl ;
  Test * t2 = new(b) Test ;
  std::cout << "Test structure 2 created on buffer b" << std::endl ;
  char * t3 = new(b) char[4090];
  std::cout << "New vector created on buffer b" << std::endl ;

  //I am printing addresses of the oncjects and of the memory pages
  //to check that they correspond
  std::cout << "Buffer b's Segment addresses(size): " ;
  for (Buffer::page_iterator i=b->begin(); i!= b->end(); i++) { 
    std::cout << std::hex << (*i)->address() 
	 << "(" << std::dec << (*i)->usedSize()  
	 << ") " ;
  }
  std::cout << std::endl ;
  std::cout << "Object addresses(size): " 
       << std::hex << t1 << "(" << std::dec << sizeof(Test) << ") " 
       << std::hex << t2 << "(" << std::dec << sizeof(Test) << ") " 
       << std::hex << (void *)t3 << "(" << std::dec << sizeof(char[4090]) << ")"
       << std::endl ;

  std::cout << "Buffer b's Segment capacities: " ;
  for (Buffer::page_iterator i=b->begin(); i!= b->end(); i++) { 
    std::cout << std::dec << (*i)->capacity() << " " ;
  }
  std::cout << std::endl << std::endl ;

  //Now I create another buffer 
  Buffer * b1 = new Buffer(pool) ; 
  std::cout << "New buffer b1 created: size=" << b1->size() << std::endl;

  //First I try to allocate a vector larger than the pagesize
  try {
    char * t = new(b1) char[pool->pageSize()+1];    
    std::cout << "New vector allocated at address " << (void *)t << std::endl ;
  }
  catch (ROSException &e) {
    if (e.getErrorId()==BufferManagementException::TOO_LARGE_RESERVATION) {
      std::cout << "Cannot allocate " << (pool->pageSize()+1) << " bytes!" << std::endl ; 
    }
    else {
      std::cout << "Unknown exception:   " << e << std::endl ;
      std::cout << "Read code " << e.getErrorId() << "  expected " << BufferManagementException::TOO_LARGE_RESERVATION << std::endl ;
    }
  }
  std::cout << "New b1 size: " << b1->size() << std::endl ; 

  //then I try to instantiate new objects as long as there are free pages
  bool tryFlag = true ;
  for (int i=0; tryFlag; i++ ) {
    try {
      char * t = new(b1) char[4090];    
      std::cout << i << ": New vector of 4090 bytes allocated at address " << (void *)t << std::endl ;
      std::cout << "New b1 size: " << b1->size() << std::endl ; 
    }
    catch (MemoryPoolException &e) {
      if (e.getErrorId()==MemoryPoolException::NOPAGESAVAILABLE) {
	      std::cout << "No pages available anymore!" << std::endl ;
	if (pool->isPageAvailable()) 
	  std::cout << "ERROR: the MemoryPool seem to still have available pages!" << std::endl ;  
      }
      else {
	      std::cout << e << std::endl;
      }
      tryFlag = false ;
    }
    catch (ROSException &e) {
      std::cout << e << std::endl ;
      tryFlag = false ;
    }
  }
  
  //delete first buffer and check that the allocated pages are freed 
  delete b ;
  std::cout << "Deleted buffer b" << std::endl ;
  if (! pool->isPageAvailable()) 
    std::cout << "ERROR: the MemoryPool did not get his pages back!" << std::endl ;

  //Now check how many more pages we can allocate 
  //(Should be the number of pages allocated by the first buffer!)
  tryFlag = true ;
  for (int i=0; tryFlag; i++ ) {
    try {
      char * t = new(b1) char[4090];    
      std::cout << i << ": New vector of 4090 bytes allocated at address " << (void *)t << std::endl ;
      std::cout << "New b1 size: " << b1->size() << std::endl ; 
    }
    catch (MemoryPoolException &e) {
      if (e.getErrorId()==MemoryPoolException::NOPAGESAVAILABLE) {
	      std::cout << "No pages available anymore!" << std::endl ;
	if (pool->isPageAvailable()) 
	  std::cout << "ERROR: the MemoryPool seem to still have available pages!" << std::endl ;  
      }
      else {
	      std::cout << e << std::endl;
      }
      tryFlag = false ;
    }
    catch (ROSException &e) {
      std::cout << e << std::endl ;
      tryFlag = false ;
    }
  }

  

  return 0;
}
