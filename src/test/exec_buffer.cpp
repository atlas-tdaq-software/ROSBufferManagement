/****************************************************************/
/*								*/
/*  file exec_buffer.cpp					*/
/*								*/
/*  exerciser for the buffer class				*/
/*								*/
/*  The program allows to:					*/
/*  . exercise the methods of the Buffer Class			*/
/*  . measure the timing performance				*/
/*								*/
/*								*/
/****************************************************************/

//#include <iostream.h>
//#include <iomanip.h>
#include "ROSGetInput/get_input.h"

#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"

using namespace ROS;

#include "rcc_time_stamp/tstamp.h"

enum {CREATEPOOL = 0x01,
      CREATEBUFFER,
      RESERVESPACE,
      APPEND,
      COPYINNEWBUFFER,
      COPYINEXISTINGBUFFER,
      CLEARBUFFER,
      DESTROYBUFFER,
      DUMPBUFFER,
      DESTROYPOOL,
      PRINTBUFFERARRAY,
      PRINTPOOLARRAY,
      TIMING,
      TIMING2,
      HELP,
      QUIT = 100
};

#define MAX_POOLS 10 
#define MAX_BUFFERS 10
#define MAX_J 1000

/*******************/
int main(void)
/*******************/ 
{
  char option;
  int quit_flag;
  int poolType; 

  MemoryPool * memPool[MAX_POOLS] = {0};
  int poolNo;
  int noPages, pagesize;

  Buffer * b[MAX_BUFFERS] = {0};
  int bufPoolNo[MAX_BUFFERS] = {0};
  int bufNo;
  int bufNoOne, bufNoTwo;
  int space;
  
  tstamp ts1;
  tstamp ts2;
  float delta;
  double nsecpercall;

  quit_flag = 0;

// create the underlying memory pool(s)

do {

  std::cout << std::endl << std::endl;
  std::cout << " create memory pool         : " << std::dec <<  CREATEPOOL << std::endl;
  std::cout << " create buffer (new)        : " << std::dec <<  CREATEBUFFER << std::endl;
  std::cout << " reserve space on buffer    : " << std::dec <<  RESERVESPACE << std::endl;
  std::cout << " append buffer              : " << std::dec <<  APPEND << std::endl;
  std::cout << " copy buffer in new one     : " << std::dec <<  COPYINNEWBUFFER << std::endl;
  std::cout << " copy buffer in existing one: " << std::dec <<  COPYINEXISTINGBUFFER << std::endl;
  std::cout << " clear buffer               : " << std::dec <<  CLEARBUFFER << std::endl;
  std::cout << " destroy buffer (delete)    : " << std::dec <<  DESTROYBUFFER << std::endl;
  std::cout << " print buffer               : " << std::dec <<  DUMPBUFFER << std::endl;
  std::cout << " destroy memory pool        : " << std::dec <<  DESTROYPOOL << std::endl;
  std::cout << " print buffer array         : " << std::dec <<  PRINTBUFFERARRAY << std::endl;
  std::cout << " print pool array           : " << std::dec <<  PRINTPOOLARRAY << std::endl;
  std::cout << " TIMING new/delete          : " << std::dec <<  TIMING << std::endl;
  std::cout << " TIMING append              : " << std::dec <<  TIMING2 << std::endl;
  std::cout << " HELP                       : " << std::dec <<  HELP << std::endl;
  std::cout << " QUIT                       : " << std::dec <<  QUIT << std::endl;
 
  std::cout << " ? : ";

  option = getdec(); /* get an integer */
 
  switch(option)   {
 
  case CREATEPOOL : 
    std::cout << " pool type (MALLOC = " << MemoryPool::ROSMEMORYPOOL_MALLOC <<
                         " , CMEM = " << MemoryPool::ROSMEMORYPOOL_CMEM_GFP <<
                         " , BPA = " << MemoryPool::ROSMEMORYPOOL_CMEM_BPA << ") ";
    poolType = getdecd(MemoryPool::ROSMEMORYPOOL_MALLOC);


    std::cout << " pool # = (<" << MAX_POOLS << ") ";
    poolNo = getdecd(0);

    std::cout << std::endl << " Parameters of the memory Page Pool : " << std::endl;
    std::cout << " # pages = ";
    noPages = getdecd(10);
    std::cout << " page size = ";
    pagesize = getdecd(4096);

    try {
      if (poolType == MemoryPool::ROSMEMORYPOOL_MALLOC)
        memPool[poolNo] = new MemoryPool_malloc(noPages,pagesize);
      else
        memPool[poolNo] = new MemoryPool_CMEM(noPages,pagesize,poolType);
    }
    catch (ROSException& e) {
      std::cout << " package: " << e.getPackage() << std::endl;
      std::cout << " error ID: " << e.getErrorId() << std::endl;
      std::cout << " Description: " << e.getDescription() << std::endl;
    }
    
    break;
 
  case CREATEBUFFER : 

    std::cout << " buffer # = (<" << MAX_BUFFERS << ") ";
    bufNo = getdec();

    std::cout << " in pool # = (<" << MAX_POOLS << ") ";
    poolNo = getdec();

    b[bufNo] = new Buffer(memPool[poolNo]) ;
    bufPoolNo[bufNo] = poolNo;			// remember its pool

    break;
 
  case RESERVESPACE:
    std::cout << " buffer # = (<" << MAX_BUFFERS << ") ";
    bufNo = getdec();    
    
    std::cout << " space in bytes = " ;
    space = getdec();    
    
    b[bufNo]->reserve(space);

    break;

  case APPEND : 
  {
    std::cout << " Append data of buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoTwo = getdec();

    std::cout << " from offset = ";
    u_int offset = getdecd(0);

    std::cout << " for maximum size = ";
    u_int maxSize = getdecd(b[bufNoTwo]->size());

    std::cout << " to buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoOne = getdec();
 
    // try .. 
    b[bufNoOne]->append(*b[bufNoTwo],offset,maxSize);
  }
    break;
 
  case DUMPBUFFER : 

    std::cout << " buffer # = (<" << MAX_BUFFERS << ") ";
    bufNo = getdecd(0);

    b[bufNo]->print();

    break;
 
  case COPYINNEWBUFFER : 

    std::cout << " copied buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoOne = getdec();

    std::cout << " new buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoTwo = getdec();

    std::cout << " in pool # = (<" << MAX_POOLS << ") ";
    poolNo = getdec();

    b[bufNoTwo] = new Buffer(*b[bufNoOne],memPool[poolNo]) ;
    bufPoolNo[bufNoTwo] = poolNo;			// remember its pool

    break;
 
  case COPYINEXISTINGBUFFER : 

    std::cout << " copied buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoOne = getdec();

    std::cout << " existing buffer # = (<" << MAX_BUFFERS << ") ";
    bufNoTwo = getdec();

    *b[bufNoTwo] = *b[bufNoOne];

    break;
 
  case CLEARBUFFER:
    std::cout << " buffer # = (<" << MAX_BUFFERS << ") ";
    bufNo = getdec();    
    
    b[bufNo]->clear();

    break;

  case DESTROYBUFFER : 

    std::cout << " buffer # = (<" << MAX_BUFFERS << ")";
    bufNo = getdecd(bufNo);

    delete b[bufNo];

    b[bufNo] = 0;
    bufPoolNo[bufNo] = 0;

    break;
 
  case DESTROYPOOL : 

    std::cout << " pool # = (<" << MAX_POOLS << ") ";
    poolNo = getdecd(0);

    delete memPool[poolNo];

    memPool[poolNo] = 0;

    break;
 
  case PRINTBUFFERARRAY : 

    std::cout << " buffer (pointer) in pool #  : " << std::endl;
    for (int i = 0; i < MAX_BUFFERS; i++) {
      std::cout << std::setw(4) << i << "  " << std::hex << b[i];
      if (b[i])
        std::cout << std::setw(12) << bufPoolNo[i];
      std::cout << std::endl;
    }

    break;
 
  case PRINTPOOLARRAY : 

    std::cout << " pool (pointer) array  # pages  page size " << std::endl;
    for (int i = 0; i < MAX_POOLS; i++) {
      std::cout << std::setw(5) << i << "       " << std::hex << memPool[i];
      if (memPool[i])
        std::cout << std::setw(9) << std::dec << memPool[i]->numberOfPages()
             << std::setw(11) << std::dec << memPool[i]->pageSize();
      std::cout << std::endl;
    }

    break;

  case TIMING :

    for ( int i = 0; i < MAX_POOLS; i++ ) {
      memPool[i] = new MemoryPool_CMEM (16,1024);
    }

    TS_OPEN(10000, TS_DUMMY);
    TS_START(TS_H1);

    ts_clock(&ts1);

    for ( int jloop = 0; jloop < MAX_J; jloop++) {   // just to make time longer ..

      for ( int iloop = 0; iloop < MAX_BUFFERS; iloop++) {	// create 10 buffers
  TS_RECORD(TS_H1,100);
        b[iloop] = new Buffer(memPool[iloop]);	// in different pools
  TS_RECORD(TS_H1,101);
      }

      for ( int iloop = 0; iloop < MAX_BUFFERS; iloop++) {	// delete 10
  TS_RECORD(TS_H1,108);
       delete b[iloop];
  TS_RECORD(TS_H1,109);
      }

    }

    ts_clock(&ts2);
    delta = ts_duration(ts1,ts2);

    std::cout << " # create/destroy calls = " << MAX_BUFFERS*MAX_J << std::endl;
    std::cout << " # seconds = " << delta << std::endl;

    nsecpercall = (delta/(MAX_BUFFERS*MAX_J))*1000000000;
    std::cout << " nanosecs/page for create/destroy = " << nsecpercall << std::endl;

    TS_SAVE(TS_H1, "new_file");

    for ( int i = 0; i < MAX_POOLS; i++ ) {
      delete memPool[i];
      memPool[i] = 0;
    }

    break;

  case TIMING2 :

    for ( int i = 0; i < MAX_POOLS; i++ ) {
      memPool[i] = new MemoryPool_CMEM (16,1024);
    }

    TS_OPEN(10000, TS_H1);

    TS_START(TS_H1);

    ts_clock(&ts1);

    for ( int jloop = 0; jloop < MAX_J; jloop++) {   // just to make time longer ..

  TS_RECORD(TS_H1,100);
      for ( int iloop = 0; iloop < MAX_BUFFERS; iloop++) {	// create 10 buffers
        b[iloop] = new Buffer(memPool[iloop]);			// in different pools
      }

  TS_RECORD(TS_H1,101);

      for ( int iloop = 1; iloop < MAX_BUFFERS; iloop++) {	// append 9 segments  to 0
        b[0]->append(*b[iloop]);
      }

  TS_RECORD(TS_H1,102);

      for ( int iloop = 0; iloop < MAX_BUFFERS; iloop++)	// delete 10
       delete b[iloop];

  TS_RECORD(TS_H1,103);

    }

    ts_clock(&ts2);
    delta = ts_duration(ts1,ts2);

    std::cout << " # ROS fragment loops = " << MAX_J << std::endl;
    std::cout << " # seconds = " << delta << std::endl;

    nsecpercall = (delta/(MAX_J))*1000000000;
    std::cout << " nanosecs/ROS fragment = " << nsecpercall << std::endl;

    TS_SAVE(TS_H1, "new_file");

    for ( int i = 0; i < MAX_POOLS; i++ ) {
      delete memPool[i];
      memPool[i] = 0;
    }

    break;

  case HELP :
 
    std::cout <<  " Exerciser program for the Buffer Class." << std::endl;
    std::cout <<  " The program allows to manipulate buffers in different memory pools." << std::endl;
    std::cout <<  " Memory pools can be created and deleted" << std::endl;
    std::cout <<  " and Buffers created and deleted in different memory pools." << std::endl;
    std::cout <<  " The command PRINTPOOLARRAY shows the memory pools defined." << std::endl;
    std::cout <<  " The command PRINTBUFFERARRAY shows the buffers defined." << std::endl;
    std::cout <<  " Other commands  correspond one-to-one to the methods in the Buffer Class." << std::endl;
    std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
    std::cout <<  " the basic Buffer methods: this is a bit for specialists ..." << std::endl;
 
    break;

  case QUIT :
 
    quit_flag = 1;
 
    break;
 
  default :
 
    std::cout <<  "not implemented yet" << std::endl;
 
  } /*main switch */

} while (quit_flag == 0);


  return 0;

}
